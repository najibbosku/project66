# project66

# for android user please download and install TERMUX Version 0.118.0 (118) 

https://f-droid.org/en/packages/com.termux/

# configuration for TERMUX
- pkg update
- apt upgrade
- pkg install binutils vim build-essential git imagemagick python-pip -y
- pip3 install --upgrade setuptools
- git clone https://gitlab.com/najibbosku/project66.git
- cd project66
- pip install -r termux.txt

# installation for microsoft windows



<details><summary>go to microsoft store</summary>
![MICROSOFT_STORE](https://gitlab.com/najibbosku/project66/-/wikis/uploads/4ea69ce1b7eb135f19c8152adbe6ba6c/MICROSOFT_STORE.JPG)
</details>

<details><summary>install python 3.11</summary>
![PYTHON_3.11](https://gitlab.com/najibbosku/project66/-/wikis/uploads/3ef05a041d4c9cb0151dc9ac045bb234/PYTHON_3.11.JPG)
</details>

<details><summary>go to windows power shell </summary>
![powershell](https://gitlab.com/najibbosku/project66/-/wikis/uploads/92944e95d0e4a6ded05a0f763662e6de/powershell.JPG)
</details>

<details><summary>git clone</summary>
![clone](https://gitlab.com/najibbosku/project66/-/wikis/uploads/b40302d5e0ca74585ca1002a69156bf6/clone.JPG)
</details>

cd project66

# bila dah siap install termux atau windows edit file kacang.py


- system_name = "TUKAR_NAMA_SYSTEM"
- bot_token = "TUKAR_CHATBOT_API"
- chat_id = "TUKAR_TELEGRAM_CHAT_ID"

# execute the software
- python3 kacang.py







